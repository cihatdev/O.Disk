const Disk = require('./index.min.js')
global.localStorage = require('localStorage')
const Dene = require('dene')


let Tests = {
  assign: () => {
    Disk.açar = 'değer'
    return JSON.parse(localStorage.getItem('açar')) == 'değer'
  },
  get: () => {
    localStorage.setItem('açar', '"değer"')
    return Disk.açar == 'değer'
  },
  assignAnObject: () => {
    let Nesne = {
      a: 1
    }
    Disk.açar = Nesne
    return JSON.parse(localStorage.getItem('açar')).a == Nesne.a
  },
  getAnObject: () => {
    let Nesne = {
      a: 1
    }
    Disk.açar = Nesne
    return Disk.açar.a == 1
  },
  expire: () => {
    return new Promise((res, rej)=>{
      let call = {res, rej}
      let Nesne = {
        a: 1
      }
      Disk.açar = Nesne
      Disk.expire('açar', 1)
    debugger
      let beforeExpire = Disk.açar.a == 1
      setTimeout(() => {call[beforeExpire && Disk.açar == null ? 'res' : 'rej']()}, 2000)
    })
  }
}
Dene(Tests)