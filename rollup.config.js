import {terser} from 'rollup-plugin-terser'
export default [{
        input: './index.js',
        output: {
            file: './index.min.js',
            format: 'cjs'
        },
        plugins:[
            terser()
        ]
    },
    {
        input: './disk.fs.js',
        output: {
            file: './disk.min.js',
            format: 'cjs'
        },
        plugins:[
            terser()
        ]
    },
]